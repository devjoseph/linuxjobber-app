from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User

from .models import (GoalStatus, ScrumyGoals, ScrumyHistory)
import random
# Create your views here.

def index(request):
    goal = ScrumyGoals.objects.filter(goal_name='Learn Django')
    return HttpResponse(goal)


def move_goal(request, goal_id):
    try: 
        obj = ScrumyGoals.objects.get(goal_id=goal_id) 
    except Exception as e: 
        return render(request, 'josephscrumy/exception.html', {'error': 'A record with that goal id does not exist'})
    else: 
        return HttpResponse(obj.goal_name)


def add_goal(request):
    user = User.objects.get(username='LouisOma')
    ids = random.randint(1000,9999)
    goal_status=GoalStatus.objects.get(status_name='Weekly Goal')
    ScrumyGoals.objects.create(goal_name='Keep Learning Django',goal_id=ids ,created_by = 'Louis',moved_by='Louis',owner='Louis',goal_status=goal_status,user=user)


def home(request):
    goal_name = ScrumyGoals.objects.get(goal_name='Learn Django')
    goal_id = int(ScrumyGoals.objects.get(goal_id=1))
    user = User.objects.get(username='louis')
    context = {
        'goal_name': goal_name,
        'goal_id': goal_id,
        'user': user,
    }
    return render(request, 'josephscrumy/home.html', {'context':context})


    
    


